﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milestone3
{
    class Program
    {
        static Customer Customer;
        static Dictionary<string, string> PizzaTypes;
        static Dictionary<string, string> PizzaSizes;
        static Dictionary<string, float> PizzaPrices;
        static Dictionary<string, string> DrinkTypes;

        const float DrinkPrice = 4.5f;

        static void Main(string[] args)
        {
            PizzaTypes = new Dictionary<string, string>();
            PizzaTypes.Add("1", "Meatlovers");
            PizzaTypes.Add("2", "Supreme");
            PizzaTypes.Add("3", "Hawaiian");
            PizzaTypes.Add("4", "Vegetarian");

            PizzaSizes = new Dictionary<string, string>();
            PizzaSizes.Add("1", "Small");
            PizzaSizes.Add("2", "Medium");
            PizzaSizes.Add("3", "Large");

            PizzaPrices = new Dictionary<string, float>();
            PizzaPrices.Add("1", 8);
            PizzaPrices.Add("2", 10);
            PizzaPrices.Add("3", 12);

            DrinkTypes = new Dictionary<string, string>();
            DrinkTypes.Add("1", "Coke");
            DrinkTypes.Add("2", "Fanta");
            DrinkTypes.Add("3", "Lemonade");


            Menu();
        }

        static void Menu()
        {
            Console.Clear();
            Console.WriteLine("Welcome to tasty pizza place");
            Console.WriteLine("----------------------------");
            Console.WriteLine("1. New Order");
            Console.WriteLine("2. Exit");
            Console.WriteLine("----------------------------");
            Console.Write("Enter Selection: ");
            var option = Console.ReadLine();

            if (option == "1")
                NewOrder();
            else if (option == "2")
                return;
            else Menu();

        }

        static void NewOrder()
        {
            Customer = new Customer();
            Customer.OrderDetail = new Order();
            Customer.OrderDetail.Pizzas = new List<Pizza>();
            Customer.OrderDetail.Drinks = new List<Drink>();

            Console.Clear();
            Console.Write("Please Enter Name: ");
            Customer.Name = Console.ReadLine();

            Console.Clear();
            Console.Write("Please Enter Phone: ");
            Customer.PhoneNumber = Console.ReadLine();

            OrderPizza();

        }

        static void OrderPizza()
        {
            Console.Clear();
            Console.WriteLine("Select Pizza");
            Console.WriteLine("----------------------------");
            foreach (var pizza in PizzaTypes)
            {
                Console.WriteLine(pizza.Key + ". " + pizza.Value);
            }
            Console.WriteLine("");
            Console.WriteLine("9. Cancel Order");
            Console.WriteLine("----------------------------");
            Console.Write("Enter Selection: ");
            var option = Console.ReadLine();

            if (PizzaTypes.ContainsKey(option))
            {
                Pizza pizza = new Pizza();
                pizza.Name = PizzaTypes[option];
                OrderPizzaSize(pizza);
            }
            else if (option == "9")
                Menu();
            else OrderPizza();

        }

        static void OrderPizzaSize(Pizza pizza)
        {
            Console.Clear();
            Console.WriteLine("Select Pizza Size");
            Console.WriteLine("----------------------------");
            foreach (var size in PizzaSizes)
            {
                Console.WriteLine(size.Key + ". " + size.Value);
            }
            Console.WriteLine("----------------------------");
            Console.Write("Enter Selection: ");
            var option = Console.ReadLine();
            if (PizzaSizes.ContainsKey(option))
            {
                pizza.Size = PizzaSizes[option];
                pizza.Cost = PizzaPrices[option];
                Customer.OrderDetail.Pizzas.Add(pizza);
                AddPizza();
            }
            else OrderPizzaSize(pizza);

        }

        static void AddPizza()
        {
            Console.Clear();
            Console.Write("Add Another Pizza (y/n): ");
            var option = Console.ReadLine();
            if (option == "y")
                OrderPizza();
            else if (option == "n")
                OrderDrinks();
            else AddPizza(); //
        }

        static void OrderDrinks()
        {
            Console.Clear();
            if (Customer.OrderDetail.Drinks.Count > 0)
            {
                Console.WriteLine("Already in Order");
                Console.WriteLine("****************************");
                foreach (var drink in Customer.OrderDetail.Drinks)
                {
                    Console.WriteLine("* Drink: " + drink.Name + " $" + drink.Cost);
                }
                Console.WriteLine("****************************");
                Console.WriteLine("");
            }

            Console.WriteLine("Add Drink");
            Console.WriteLine("----------------------------");
            foreach (var drink in DrinkTypes)
            {
                Console.WriteLine(drink.Key + ". " + drink.Value);
            }
            Console.WriteLine("");
            Console.WriteLine("9. Finished Ordering");
            Console.WriteLine("----------------------------");
            Console.Write("Enter Selection: ");
            var option = Console.ReadLine();
            if (DrinkTypes.ContainsKey(option))
            {
                Drink drink = new Drink();
                drink.Name = DrinkTypes[option];
                drink.Cost = DrinkPrice;
                Customer.OrderDetail.Drinks.Add(drink);

                OrderDrinks();
            }
            else if (option == "9")
                PayBill();
            else OrderDrinks();

        }

        static void PayBill()
        {
            Console.Clear();
            Console.WriteLine("Order Up!");
            Console.WriteLine("----------------------------");
            Console.WriteLine("Name: " + Customer.Name);
            Console.WriteLine("Phone: " + Customer.PhoneNumber);
            Console.WriteLine("----------------------------");

            float totalcost = 0;
            foreach (var pizza in Customer.OrderDetail.Pizzas)
            {
                Console.WriteLine("Pizza: " + pizza.Size + " " + pizza.Name + " $" + pizza.Cost);
                totalcost += pizza.Cost;
            }

            if (Customer.OrderDetail.Drinks.Count > 0)
            {
                Console.WriteLine("----------------------------");
                foreach (var drink in Customer.OrderDetail.Drinks)
                {
                    Console.WriteLine("Drink: " + drink.Name + " $" + drink.Cost);
                    totalcost += drink.Cost;
                }
            }
            Console.WriteLine("----------------------------");
            Console.WriteLine("Bill Total: $" + totalcost);
            Console.WriteLine("----------------------------");
            Console.WriteLine("");
            Console.Write("Order Paid (y/n): ");

            var option = Console.ReadLine();
            if (option == "y")
            {
                Console.Clear();
                Console.WriteLine("THANKS! Press any key to return to menu...");
                Console.ReadKey();
                Menu();
            }
            else PayBill();

        }

    }

    public class Customer
    {
        public string Name;
        public string PhoneNumber;
        public Order OrderDetail;
    }

    public class Order
    {
        public List<Pizza> Pizzas;
        public List<Drink> Drinks;
    }

    public class Pizza
    {
        public string Name;
        public string Size;
        public float Cost;
    }

    public class Drink
    {
        public string Name;
        public float Cost;
    }
}